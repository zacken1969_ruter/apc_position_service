"use strict";

//Load enviroment from file if exists
require('custom-env').env(true);

//Check for mandatory enviroment
if (!(process.env.KAFKA_CONSUMER_BROKER
    && process.env.KAFKA_PRODUCER_BROKER
    && process.env.KAFKA_SCHEMA_REGISTER
    && process.env.KAFKA_CLIENT_ID
    && process.env.KAFKA_APC_COMMAND_TOPIC
    && process.env.KAFKA_VEHICLE_POSITION_TOPIC
    && process.env.APC_AGGREGATION_TIMEOUT
    && process.env.VEHICLE_POSITION_BUFFER_SIZE
)) {
    console.log("Enviroment is missing. Exiting with code 1");
    process.exit(1);
}

//Setup consumer
const { Consumer } = require("kafka_connector");
var consumer = new Consumer({
    broker: process.env.KAFKA_CONSUMER_BROKER,
    schemaRegister: process.env.KAFKA_SCHEMA_REGISTER,
    clientId: process.env.KAFKA_CLIENT_ID,
    maxAge: 3600 * 2,
    commitOffsetsOnOverAge: true,
    commitOffsets: true,
    stageTopicLoading: true
}, [process.env.KAFKA_VEHICLE_POSITION_TOPIC, process.env.KAFKA_APC_COMMAND_TOPIC]);

//Setup internal state
const vehicleState = new Map();

//Set loaded to true when all toppics are current to avoid false matchings.
var loaded = false;
consumer.on("loaded", () => { loaded = true })

//Process incomming messages
consumer.on("message", (message) => {
    if (message.topic == process.env.KAFKA_VEHICLE_POSITION_TOPIC) updateVehiclePositionBuffer(message.value);
    if (message.topic == process.env.KAFKA_APC_COMMAND_TOPIC) collectApcMessages(message.value);
})

//Process possition messages
function updateVehiclePositionBuffer(message) {
    //Get vehicleRef
    const vehicleRef = message.EntityPartition.Key;

    //Lookup vehicle in internal state, create if non-existent
    var vehicleRecord = vehicleState.get(vehicleRef);
    if (!vehicleRecord) vehicleRecord = {
        vehicleRef: vehicleRef,
        vehiclePositions: [],
        apcRecordsForAggregation: [],
        apcAggregationTimeout: null,
        maxPassengersIn:0,
        maxPassengersOut:0,
        numberOfHistoricCountings:0
    }

    //Create vehicle position record
    const timestamp = new Date(message.Timestamp);
    const location = [message.EntityPartition.Longitude, message.EntityPartition.Latitude];
    const positionRecord = { timestamp: timestamp, location: location };

    //Update rolling possition buffer for vehicle
    vehicleRecord.vehiclePositions.push(positionRecord);
    if (vehicleRecord.vehiclePositions.length > process.env.VEHICLE_POSITION_BUFFER_SIZE) vehicleRecord.vehiclePositions.shift();

    //Create or update vehicle internal state
    vehicleState.set(vehicleRef, vehicleRecord);
}

//Process APC message
function collectApcMessages(message) {
    if (loaded) { //Check if state is current
        //Get vehicle state from internal state
        const vehicleRef = message.Command.VehicleRef;
        var vehicleRecord = vehicleState.get(vehicleRef);
        if (vehicleRecord) { //Only process if we have positions

            //Reccord APC Message in incomming apc queue in internal state
            vehicleRecord.apcRecordsForAggregation.push(message);

            //Setup aggregation timeout
            if (vehicleRecord.apcAggregationTimeout) clearTimeout(vehicleRecord.apcAggregationTimeout);
            vehicleRecord.apcAggregationTimeout = setTimeout(() => {
                aggregateApcMessages(vehicleRef);
            }, process.env.APC_AGGREGATION_TIMEOUT * 1000)

            //Update vehicle internal state
            vehicleState.set(vehicleRef, vehicleRecord);
        }
    }
}

function aggregateApcMessages(vehicleRef) {
    //Get internal vehicle state
    var vehicleRecord = vehicleState.get(vehicleRef);
    if (vehicleRecord) {//Safeguard

        //Setup aggregation attributes
        var minTimestamp = null;
        var maxTimestamp = null;
        var countings = 0;
        var passengersIn = 0;
        var passengersOut = 0;
        var doors = [];

        //Procces collected countings
        vehicleRecord.apcRecordsForAggregation.forEach(apcCount => {

            //Record number of countings
            countings++;

            //Track timestamps
            var timestamp = new Date(apcCount.Timestamp);
            if (minTimestamp == null) {
                minTimestamp = timestamp;
                maxTimestamp = timestamp;
            } else {
                if (minTimestamp > timestamp) minTimestamp = timestamp;
                if (maxTimestamp < timestamp) maxTimestamp = timestamp;
            }

            //Aggregate countings
            apcCount.Command.PassengerCountings.forEach(doorCount => {
                if (doorCount.PassengerClass != "BIKE") {
                    passengersIn = passengersIn + doorCount.DoorPassengerIn;
                    passengersOut = passengersOut + doorCount.DoorPassengerOut;
                }
            })

            //Track doors
            var doorId = apcCount.Command.DoorId;
            if (!doors.includes(doorId)) doors.push(doorId);
        });

        //Lookup position
        var apcLocation = null;
        var apcLocationTimestamp = null;
        vehicleRecord.vehiclePositions.forEach(position => {
            if (position.timestamp < minTimestamp) {
                apcLocation = position.location;
                apcLocationTimestamp=position.timestamp;
            }
        })
        if(apcLocationTimestamp) apcLocationTimestamp=apcLocationTimestamp.toISOString();

        //Calulate quality indicators
        if(vehicleRecord.maxPassengersIn<passengersIn) vehicleRecord.maxPassengersIn=passengersIn;
        if(vehicleRecord.maxPassengersOut<passengersOut) vehicleRecord.maxPassengersOut=passengersOut;
        vehicleRecord.numberOfHistoricCountings++;

        //Output result
        var output = {
            vehicleRef: vehicleRef,
            location: apcLocation,
            locationTimestamp:apcLocationTimestamp,
            minTimestamp: minTimestamp.toISOString(),
            maxTimestamp: maxTimestamp.toISOString(),
            periodeLengthInSeconds:Math.round((maxTimestamp-minTimestamp)/1000),
            passengersIn: passengersIn,
            passengersOut: passengersOut,
            numberOfCountings: countings,
            doors: doors,
            numberOfDoors:doors.length,
            maxPassengersIn:vehicleRecord.maxPassengersIn,
            maxPassengersOut:vehicleRecord.maxPassengersOut
        }
        //if((vehicleRecord.maxPassengersIn==0)&&(vehicleRecord.maxPassengersOut==0)) 
        console.log(JSON.stringify(output));

        // Clear countings from internal vehicle state
        vehicleRecord.apcRecordsForAggregation = [];
        vehicleRecord.apcAggregationTimeout = null;
    }
}