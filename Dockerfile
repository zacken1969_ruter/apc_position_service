FROM node:10-alpine as base1

RUN apk --no-cache add \
      bash \
      g++ \
      ca-certificates \
      lz4-dev \
      musl-dev \
      cyrus-sasl-dev \
      openssl-dev \
      make \
      python \
      git 

RUN apk add --no-cache --virtual .build-deps gcc zlib-dev libc-dev bsd-compat-headers py-setuptools bash

WORKDIR /usr/src/app
# COPY . /usr/src/app/
COPY package* /usr/src/app/

RUN npm install
# If you are building your code for production 
# RUN npm ci --only=production

FROM node:10-alpine

RUN apk --no-cache add \
      libsasl \
      lz4-libs

WORKDIR /usr/src/app
COPY --from=base1 /usr/src/app/node_modules ./node_modules
COPY . .
COPY .env .env
# Bundle app source
EXPOSE 3001
CMD [ "node", "index.js" ]